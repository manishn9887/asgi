from django.db import models

class Product(models.Model):
    title = models.CharField(max_length = 180)
    description = models.TextField ()
    price = models.IntegerField(default = False)
    seller = models.CharField(max_length = 180)

    def __str__(self):
        return self.title
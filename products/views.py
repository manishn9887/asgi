from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Product
from .serializers import ProductSerializer

class ProductListApiView(APIView):
    def get(self, request):
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        data = request.data
        serializer = ProductSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProductDetailApiView(APIView):
    def get_object(self, product_id):
        try:
            return Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return None

    def get(self, request,product_id):
        product_instance = self.get_object(product_id)
        if not product_instance:
            return Response({"res": "Product does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = ProductSerializer(product_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, product_id):
        product_instance = self.get_object(product_id)
        if not product_instance:
            return Response({"res": "Product does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        productDict = ProductSerializer(product_instance).data
        data= request.data
        productDict.update(data)
        serializer = ProductSerializer(instance = product_instance, data=productDict, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, product_id):
        product_instance = self.get_object(product_id)
        if not product_instance:
            return Response(
                {"res": "Product does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )
        product_instance.delete()
        return Response({"res": "Product deleted!"},status=status.HTTP_200_OK)
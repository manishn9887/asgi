from rest_framework import serializers
from .models import Product

class ProductSerializer(serializers.ModelSerializer):
    title = serializers.CharField(allow_blank=False)
    price = serializers.IntegerField()

    def validate_price(self,price):
        if price < 100 or price > 1000000:
            raise serializers.ValidationError("Price is not valid. Enter a price in range [100, 1000000]")
        return price

    class Meta:
        model = Product
        fields = ["title", "description", "price", "seller"]

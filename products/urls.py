from django.urls import path
from .views import ProductListApiView
from .views import ProductDetailApiView

urlpatterns = [
    path('', ProductListApiView.as_view()),
    path('<int:product_id>/', ProductDetailApiView.as_view()),
]